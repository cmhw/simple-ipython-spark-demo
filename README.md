# Simple IPython Spark Demo #

## README ##

### Overview ###

* This project contains a simple example of using Apache Spark and IPython Notebook together.

### Pre-requisites ###

* Python 2.7+ and IPython (recommend [Anaconda distribution](http://continuum.io/downloads) which includes Spark-compatible versions of these).
* Linux operating system e.g. Ubuntu, Mint etc.
* [Apache Spark version 1.2](http://spark.apache.org/docs/1.2.0/index.html) or later.
* Make sure your `SPARK_HOME` environment variable is set to your Spark home directory.

### Data for sample notebook ###

* The sample notebook performs some simple text processing on files downloaded from [Project Gutenberg](https://www.gutenberg.org/).
* Check the [Project Gutenberg terms of use](http://www.gutenberg.org/wiki/Gutenberg:Terms_of_Use) and the [Project Gutenberg licence](http://www.gutenberg.org/wiki/Gutenberg:The_Project_Gutenberg_License).
* If you are able to comply with these terms, download one of the suggested text files and place it in the `/data` directory with a suitable file-name.  For example:
> * `shakespeare.txt` ([complete works of Shakespeare](http://www.gutenberg.org/ebooks/100))
> * `marcus-aurelius-meditations.txt` ([Meditations of Marcus Aurelius](http://www.gutenberg.org/ebooks/2680))
* Of course, you can use any text file you like, but these longer texts give us a reasonable set of data to play with here.

### Starting the notebook server ###

* There is a sample Linux shell-script `start_notebook.sh` containing the command to start IPython Notebok with Spark.
* Or you can just run the following command yourself in a Linux terminal shell:

```
    # Assumes Spark is installed in $SPARK_HOME
    IPYTHON=1 IPYTHON_OPTS="notebook" $SPARK_HOME/bin/pyspark


```
* This should launch the IPython Notebook server with Spark and open the default URL: [http:127.0.0.1:8888](http:127.0.0.1:8888) in your browser.
* You should now be able to create a new notebook for your Spark code.

### Working on the sample notebook ###

* The file `IPythonSpark-1.ipynb` contains a sample notebook that performs some basic text-processing using Spark's Python API on text files downloaded from the [Gutenberg Project](https://www.gutenberg.org/) as described above.
* Open the IPythonSpark-1 notebook via the browser and play around with the code to see how it works.

### Prime Number Checker###

* Just for fun, I included the prime number checker from Benjamin Bengfort's (District Data Labs) helpful blog post [Getting Started With Spark In Python](https://districtdatalabs.silvrback.com/getting-started-with-spark-in-python).